#!/bin/sh


# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.

if [ ! -f /home/vagrant/extra_homestead_software_installed ]; then
	echo 'installing extra software'

    # update packages
    echo "First update packages please wait..."
    sudo apt-get clean
    sudo apt-get update

    # install new packages
    echo "Then install extra packages please wait..."
    sudo apt-get -y install htop
    sudo apt-get -y install mc

    ## Install phpMyAdmin
    echo "Installing phpMyAdmin please wait..."
    composer -g config repositories.phpmyadmin composer https://www.phpmyadmin.net/packages.json
    composer create-project phpmyadmin/phpmyadmin --no-dev
    ln -s /home/vagrant/phpmyadmin/ /home/vagrant/code/phpmyadmin

    #
    # remember that the extra software is installed
    #
    echo "Success! Extra software is installed."


    touch /home/vagrant/extra_homestead_software_installed
else
    echo "Extra software already installed... moving on..."
fi

## show versions & IPs
echo ""
    echo 'BOX INFO'
    php -v | sed -n 1p
    mysql --version
    psql --version
    nginx -v  2>&1 | grep version
    redis-cli info server | grep redis_version
    ruby -v
    echo 'node' `node -v`
    echo 'npm' `npm -v`
    echo '----------';